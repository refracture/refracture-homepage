// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import VueKeybindings from 'vue-keybindings'

Vue.config.productionTip = false

Vue.use(VueKeybindings, {
  alias: {
      space: 'space',
      escape: 'esc'
  }
})

/* eslint-disable no-new */
window._VueInstance = new Vue({
  el: '#app',
  router,
  components: {
    App
  },
  template: '<App/>'
})
