export default {
    pages: [
        {
            display: 'Music',
            to: 'music'
        },
        {
            display: 'Embed',
            to: 'embed'
        },
        {
            display: 'Intern',
            to: 'intern'
        }
    ]
};
